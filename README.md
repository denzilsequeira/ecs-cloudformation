## Overview of the Architecture

I've created Private and Public Subnets in Multi-AZ (2 each). The Private Subnets host the ECS Service which is deployed leveraging AWS Fargate.
The containers deployed are accessible using via the ELB deployed in the Public Subnet. The NateGateway helps with Internet for resources in the 
Private Subnet


![Alt text](https://bitbucket.org/repo/E6E4nRz/images/6240298-WhatsApp%20Image%202020-03-15%20at%2011.59.36%20PM.jpeg)

---

## Steps to create the environment using CloudFormation



**CloudFormation Architecture.**

To Make sure the stacks are reusable, I've made use Nested Stacks. Nested Stacks helps multiple stacks be linked within a single root yaml file.

Using the following commands a packaged yaml file is generated with the linked yaml files uploaded to a given S3 Bucket. The last command deploys
the stacks with the Input Parameters. 

The templates takes only a single external paramater which is the StackName, here is this case it's named Test

>
> **aws cloudformation package --template-file root.yaml  --output-template packaged_root.yaml --s3-bucket sample-server-test**
>
> **aws cloudformation deploy --template-file packaged_root.yaml --stack-name Test  --capabilities CAPABILITY_AUTO_EXPAND CAPABILITY_IAM  --parameter-overrides StackName=Test**
>


